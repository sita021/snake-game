# Snake Game

Hello. This is a snake game made for testing purposes.
Game is far from done, but i gave my best to create it.

## Development road

I didn't know where to start actually, I never used HTML canvas. Game is made with HTML5, CSS3, and vanilla JS.
Step by step, snake grew from a scratch into a game.. well it's not perfect but hey it works 

## Main Requirements

I tried to meet the main requirements for the game. However, I managed to make levels with walls, and animation around 10 to 20 fps.
There is a code which run game on 60fps, but there are a lot of bugs i can't fix so i commented it out.

## Optional requirements

Milestones: "c" and "d" are completed. I am working on milestone "e", but I won't complete it on time.