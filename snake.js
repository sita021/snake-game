/** Use @type {HTMLCanvasElement} for VSCode canvas intelisense */
const cvs = /** @type {HTMLCanvasElement} */ (document.getElementById("snake"));
const ctx = cvs.getContext("2d");

// let lastRenderTime = 0;
// const SNAKE_SPEED = 10;

// window.onload = gameLoop;

// function gameLoop(currentTime) {
//   window.requestAnimationFrame(gameLoop);
//   const secondsSinceLastRender = (currentTime - lastRenderTime) / 1000;
//   if (secondsSinceLastRender < 1 / SNAKE_SPEED) return;
//   lastRenderTime = currentTime;
//   draw();
// }

// window.requestAnimationFrame(gameLoop);

// create the unit
const box = 32;
// create the score
let score = 0;
// create the snake
let snake = [];
// The first element of snake array is our snakehead
snake[0] = {
  x: 9 * box,
  y: 10 * box,
};

// ## Milestone 6c ## //
// Store previous score in session
document.getElementById("previous-score").innerHTML = sessionStorage.getItem(
  "previous-score"
);

// Create the food on random position
let food = {
  x: Math.floor(Math.random() * 17 + 1) * box,
  y: Math.floor(Math.random() * 15 + 3) * box,
};
// ## Milestone 6d ## //
// Chance to spawn bonus food
let randomSpawn = Math.floor(Math.random() * 10);
// Create the bonus food on random position
let bonusFood = {
  x: Math.floor(Math.random() * 17 + 1) * box,
  y: Math.floor(Math.random() * 15 + 3) * box,
};

// load images
const groundImg = new Image();
groundImg.src = "img/ground.png";
const foodImg = new Image();
foodImg.src = "img/food.png";
const bonusFoodImg = new Image();
bonusFoodImg.src = "img/bonus_food.png";

// Control the snake
document.addEventListener("keydown", direction);
// snake direction
let d;
// Define default controls on keyboard
function direction(event) {
  if (event.keyCode == 37 && d != "RIGHT") {
    d = "LEFT";
  } else if (event.keyCode == 38 && d != "DOWN") {
    d = "UP";
  } else if (event.keyCode == 39 && d != "LEFT") {
    d = "RIGHT";
  } else if (event.keyCode == 40 && d != "UP") {
    d = "DOWN";
  }
}
// ## Milestone 6e ## //
// Let user modify controls
function changeControls() {
  let newControlsForm = document.getElementById("new-controls");
  if (newControlsForm.style.display === "block") {
    newControlsForm.style.display = "none";
  } else {
    newControlsForm.style.display = "block";
  }
}

function draw() {
  // Draw court field canvas background image
  ctx.clearRect(0, 0, cvs.width, cvs.height);
  ctx.drawImage(groundImg, 0, 0);
  // Draw snake
  for (let i = 0; i < snake.length; i++) {
    // Color for the snakes head and tail
    ctx.fillStyle = i == 0 ? "#e74927" : "white";
    ctx.fillRect(snake[i].x, snake[i].y, box, box);

    ctx.strokeStyle = "red";
    ctx.strokeRect(snake[i].x, snake[i].y, box, box);
  }
  // Draw food
  ctx.drawImage(foodImg, food.x, food.y);
  // Draw bonus food occasionally
  if (randomSpawn > 4) {
    ctx.drawImage(bonusFoodImg, bonusFood.x, bonusFood.y);
  }
  // Draw score
  ctx.fillStyle = "white";
  ctx.font = "32px Helvetica";
  ctx.fillText(score, 3 * box, 1.6 * box);
  // Old head position
  let snakeX = snake[0].x;
  let snakeY = snake[0].y;
  // Direction
  if (d == "LEFT") snakeX -= box;
  if (d == "UP") snakeY -= box;
  if (d == "RIGHT") snakeX += box;
  if (d == "DOWN") snakeY += box;
  // Check if snake ate food
  let foodEaten = snakeX == food.x && snakeY == food.y;
  let bonusFoodEaten = snakeX == bonusFood.x && snakeY == bonusFood.y;

  if (foodEaten || bonusFoodEaten) {
    // Increment score if food is eaten
    if (foodEaten) {
      score++;
    } else {
      // foodEaten is false, bonus food is eaten
      score += 2;
    }
    // New location
    food = {
      x: Math.floor(Math.random() * 17 + 1) * box,
      y: Math.floor(Math.random() * 15 + 3) * box,
    };
    bonusFood = {
      x: Math.floor(Math.random() * 17 + 1) * box,
      y: Math.floor(Math.random() * 15 + 3) * box,
    };
    // If we eat food, generate new chance for random food
    randomSpawn = Math.floor(Math.random() * 10);
  } else {
    // remove the tail
    snake.pop();
  }

  // Add new head
  let newHead = {
    x: snakeX,
    y: snakeY,
  };

  function collision(head, array) {
    // If main axis and cross axis match, it's a collision
    for (let i = 0; i < array.length; i++) {
      if (head.x == array[i].x && head.y == array[i].y) {
        return true;
      }
    }
    return false;
  }
  // Game over rules, wall and self collision
  if (
    snakeX < box ||
    snakeX > 17 * box ||
    snakeY < 3 * box ||
    snakeY > 17 * box ||
    collision(newHead, snake)
  ) {
    clearInterval(game);
    ctx.textAlign = "center";
    ctx.fillText(`Game over!`, cvs.width / 2, cvs.height / 2.5);
    ctx.fillText(`Your score is: ${score}`, cvs.width / 2, cvs.height / 2);
    sessionStorage.setItem("previous-score", `${score}`);
  }

  snake.unshift(newHead);
}

// Call function name every ms defined in function parameter
let game = setInterval(draw, 100);
